/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio7;

/**
 *
 * @author ederj
 */
public class Notas {

    private int filaAlumnos;
    private int columnaNotas;
    private double[][] matrizNotaxAlumno;

    public Notas(int filaAlumnos) {
        this.filaAlumnos = filaAlumnos;
        this.columnaNotas = 5;
        this.matrizNotaxAlumno = new double[filaAlumnos][columnaNotas];
    }

    public int getFilaAlumnos() {
        return filaAlumnos;
    }

    public void setFilaAlumnos(int filaAlumnos) {
        this.filaAlumnos = filaAlumnos;
    }

    public int getColumnaNotas() {
        return columnaNotas;
    }

    public void setColumnaNotas(int columnaNotas) {
        this.columnaNotas = columnaNotas;
    }

    public void LLenarMatriz() {
        for (int i = 0; i < filaAlumnos; i++) {
            for (int j = 1; j < columnaNotas; j++) {
                matrizNotaxAlumno[i][j] = Math.random() *19  + 1;
            }
        }

    }

    public Object[][] MostrarMatriz() {
        Object[][] matriz = new Object[filaAlumnos][columnaNotas];
        for (int i = 0; i < filaAlumnos; i++) {
            for (int j = 0; j < columnaNotas; j++) {
                if (j == 0) {
                    matriz[i][j] = "Alumno " + (i + 1);
                } else {
                    matriz[i][j] = String.format("%.2f", matrizNotaxAlumno[i][j]);
                }
            }
        }
        return matriz;
    }

    public String[] titulos() {
        String[] titulos = new String[5];
        titulos[0] = "Alumno";
        titulos[1] = "Exámen 1";
        titulos[2] = "Exámen 2";
        titulos[3] = "Exámen 3";
        titulos[4] = "Exámen 4";
        return titulos;
    }

    public double[] promedioxAlumnos() {
        double[] promedio = new double[filaAlumnos];
        double suma;
        for (int i = 0; i < filaAlumnos; i++) {
            suma = 0;
            for (int j = 1; j < columnaNotas; j++) {
                suma = suma + matrizNotaxAlumno[i][j];
            }
            promedio[i] = suma / (columnaNotas - 1);

        }
        return promedio;
    }

    public Object[][] MostrarPromedioxAlumno() {
        Object[][] matriz = new Object[filaAlumnos][2];
        double[] promedio = promedioxAlumnos();
        for (int i = 0; i < filaAlumnos; i++) {
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    matriz[i][j] = "Alumno " + (i + 1);
                } else {
                    matriz[i][j] = String.format("%.2f", promedio[i]);
                }
            }
        }
        return matriz;
    }

    public double[] promedioxNotas() {
        double[] promedio = new double[columnaNotas - 1];
        double suma;
        for (int j = 1; j < columnaNotas; j++) {

            suma = 0;
            for (int i = 0; i < filaAlumnos; i++) {
                suma = suma + matrizNotaxAlumno[i][j];
            }
            promedio[j - 1] = suma / (filaAlumnos);

        }
        return promedio;
    }

    public Object[][] MostrarPromedioxNotas() {
        Object[][] matriz = new Object[columnaNotas - 1][2];
        double[] promedio = promedioxNotas();
        for (int i = 0; i < columnaNotas - 1; i++) {
            for (int j = 0; j < 2; j++) {
                if (j == 0) {
                    matriz[i][j] = "Examen " + (i + 1);
                } else {
                    matriz[i][j] = String.format("%.2f", promedio[i]);
                }
            }
        }
        return matriz;
    }

    public String MejorAlumno() {
        String alumno = "";
        double[] promedio = promedioxAlumnos();
        double mayor = 0;
        int may = 0;
        for (int i = 0; i < filaAlumnos; i++) {
            if (mayor < promedio[i]) {
                mayor = promedio[i];
                may = i + 1;
            }
        }
        alumno = "Alumno " + may;
        return alumno;

    }
    
        public String MejorNota() {
        String nota = "";
        double[] promedio = promedioxNotas();
        double mayor = 0;
        int may = 0;
        for (int i = 0; i < columnaNotas - 1; i++) {
            if (mayor < promedio[i]) {
                mayor = promedio[i];
                may = i + 1;
            }
        }
        nota = "Examen " + may;
        return nota;

    }
}
